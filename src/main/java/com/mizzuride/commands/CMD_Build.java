package com.mizzuride.commands;

import com.mizzuride.data.Data;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_Build implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player) {
            final Player player = (Player) sender;

            if(player.hasPermission("mizz.dev")) {
                if(args.length == 0) {
                    if(!Data.getBuildModeMembers().contains(player)) {
                        Data.getBuildModeMembers().add(player);
                        player.sendMessage(Data.getPrefix() + "§8Du bist nun im Buildmodus!");
                    } else {
                        Data.getBuildModeMembers().remove(player);
                        player.sendMessage(Data.getPrefix() + "§8Du bist nun nicht mehr im Buildmodus!");
                    }
                }
            }
        }

        return false;
    }
}
