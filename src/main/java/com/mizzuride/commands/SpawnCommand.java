package com.mizzuride.commands;

import com.mizzuride.data.Data;
import com.mizzuride.manager.LocationManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player) {
            final Player player = (Player) sender;
            if(player.isOp() || player.hasPermission("mizz.setspawn") || player.hasPermission("mizz.*")) {
                if(args.length == 0) {
                    new LocationManager().setLocation("SPAWN", player.getLocation());
                    player.sendMessage(Data.getPrefix() + "§3Der LobbySpawn wurde erfolgreich gesetzt!");
                }
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§3You have to be a player to set the lobby spawn!");
        }

        return false;
    }
}
