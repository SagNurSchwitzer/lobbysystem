package com.mizzuride.data;

import com.mizzuride.utils.InventoryCreator;
import com.mizzuride.utils.ItemCreator;
import org.bson.Document;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class Data {

    private static String prefix;
    private static ItemStack minewar = new ItemCreator(Material.IRON_SWORD, 1, "§2§lMineWar").build();
    private static ItemStack spawn = new ItemCreator(Material.FIREBALL, 1, "§4§lSpawn").build();
    private static ItemStack jumpnrun = new ItemCreator(Material.FIREBALL, 1, "§4§lJump'n'run").build();
    private static ItemStack uhcmeetup = new ItemCreator(Material.DIAMOND_CHESTPLATE, 1, "§5§lUHC-Meetup").build();

    private static ItemStack hideItem = new ItemCreator(Material.BLAZE_ROD, 1, "§2§lSpieler Status").build();
    private static ItemStack compassItem = new ItemCreator(Material.COMPASS, 1, "§3§lNavigator").build();

    private static Inventory compassInventory = new InventoryCreator("§3§lNavigator", 27).build();

    private static ArrayList<Player> buildModeMembers = new ArrayList<Player>();
    private static HashMap<String, Integer> coinsCache = new HashMap<>();
    private static HashMap<String, ArrayList<Document>> friendCache = new HashMap<>();

    private static String[] playerList = null;

    public static void setPrefix(String prefix) {
        Data.prefix = prefix;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static ArrayList<Player> getBuildModeMembers() {
        return buildModeMembers;
    }

    public static void loadInventories() {
        compassInventory.setItem(4, getMinewar());
        compassInventory.setItem(11, getSpawn());
        compassInventory.setItem(15, getJumpnrun());
        compassInventory.setItem(22, getUhcmeetup());
    }

    public static String getRankWithColor(final Player player) {
        if(player.hasPermission("mizz.admin")) {
            return "§4Admin";
        } else if(player.hasPermission("mizz.developer")) {
            return "§bDeveloper";
        } else if(player.hasPermission("mizz.content")) {
            return "§cContent";
        } else if(player.hasPermission("mizz.moderator")) {
            return "§cModerator";
        } else if(player.hasPermission("mizz.supporter")) {
            return "§2Supporter";
        } else if(player.hasPermission("mizz.youtuber")) {
            return "§5YouTuber";
        } else if(player.hasPermission("mizz.premium")) {
            return "§6Premium";
        } else if(player.hasPermission("mizz.spieler")) {
            return "§8Spieler";
        } else {
            return "§8Spieler";
        }
    }

    public static String getRankWithoutColor(final Player player) {
        if(player.hasPermission("mizz.admin")) {
            return "Admin";
        } else if(player.hasPermission("mizz.developer")) {
            return "Developer";
        } else if(player.hasPermission("mizz.content")) {
            return "Content";
        } else if(player.hasPermission("mizz.moderator")) {
            return "Moderator";
        } else if(player.hasPermission("mizz.supporter")) {
            return "Supporter";
        } else if(player.hasPermission("mizz.youtuber")) {
            return "YouTuber";
        } else if(player.hasPermission("mizz.premium")) {
            return "Premium";
        } else if(player.hasPermission("mizz.spieler")) {
            return "Spieler";
        } else {
            return "Spieler";
        }
    }

    public static ItemStack getJumpnrun() {
        return jumpnrun;
    }

    public static ItemStack getMinewar() {
        return minewar;
    }

    public static ItemStack getSpawn() {
        return spawn;
    }

    public static ItemStack getUhcmeetup() {
        return uhcmeetup;
    }

    public static ItemStack getCompassItem() {
        return compassItem;
    }

    public static ItemStack getHideItem() {
        return hideItem;
    }

    public static Inventory getCompassInventory() {
        return compassInventory;
    }

    public static HashMap<String, Integer> getCoinsCache() {
        return coinsCache;
    }

    public static HashMap<String, ArrayList<Document>> getFriendCache() {
        return friendCache;
    }

    public static String[] getPlayerList() {
        return playerList;
    }

    public static void setPlayerList(String[] playerList) {
        Data.playerList = playerList;
    }
}
