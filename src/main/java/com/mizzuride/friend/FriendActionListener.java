package com.mizzuride.friend;

import com.mizzuride.data.Data;
import com.mizzuride.lobby.Lobby;
import com.mizzuride.utils.InventoryCreator;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class FriendActionListener implements Listener {

    @EventHandler
    public void onInt(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(event.hasItem()) {
                if(event.getItem().getType() == Material.SKULL_ITEM) {
                    Inventory friendInventory = Bukkit.createInventory(null, 6*9, "§c§lFreunde");

                    Lobby.getInstance().getFriendManager().sendPluginMessage();

                    ArrayList<Document> friends = Data.getFriendCache().get(player.getName());
                    for(int i = 0; i < friends.size(); i++) {
                        if(friends.get(i).getString("state") != "request") {
                            boolean isOnline = false;
                            if(Arrays.asList(Data.getPlayerList()).contains(friends.get(i).getString("username"))) {
                                isOnline = true;
                            }


                            ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                            SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
                            if(isOnline) {
                                skullMeta.setDisplayName("§2" + friends.get(i).getString("username"));
                            } else {
                                skullMeta.setDisplayName("§c" + friends.get(i).getString("username"));
                            }
                            skullMeta.setOwner(friends.get(i).getString("username"));
                            itemStack.setItemMeta(skullMeta);


                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInv(final InventoryClickEvent event) {

    }
}
