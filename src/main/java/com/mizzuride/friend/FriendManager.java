package com.mizzuride.friend;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mizzuride.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class FriendManager {

    public ItemStack getFriendItem(final Player player) {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setDisplayName("§c§lFreunde");
        skullMeta.setOwner(player.getName());
        itemStack.setItemMeta(skullMeta);
        return itemStack;
    }

    public void sendPluginMessage() {
        Bukkit.getScheduler().scheduleAsyncDelayedTask(Lobby.getInstance(), new Runnable() {
            @Override
            public void run() {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();

                try {
                    out.writeUTF("PlayerList");
                    out.writeUTF("ALL");

                    Bukkit.getServer().sendPluginMessage(Lobby.getInstance(), "BungeeCord", out.toByteArray());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
