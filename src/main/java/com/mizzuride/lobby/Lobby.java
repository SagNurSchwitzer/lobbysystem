package com.mizzuride.lobby;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.mizzuriapi.manager.DatabaseManager;
import com.mizzuride.commands.CMD_Build;
import com.mizzuride.commands.SetLocationCommand;
import com.mizzuride.commands.SpawnCommand;
import com.mizzuride.data.Data;
import com.mizzuride.friend.FriendManager;
import com.mizzuride.manager.*;
import com.mizzuride.pluginmessaging.PluginMessageReceivedListener;
import com.mizzuride.utils.PlayerMetaData;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.LoggerFactory;

public class Lobby extends JavaPlugin {

    private static Lobby instance;
    private PlayerMetaData playerMetaData;
    private DatabaseManager databaseManager;
    private FriendManager friendManager;

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "Das LobbySystem wird gestartet...");
        init();

        for(int i = 0; i < Bukkit.getWorlds().size(); i++) {
            Bukkit.getWorlds().get(i).setGameRuleValue("doDaylightCycle", "false");
            Bukkit.getWorlds().get(i).setGameRuleValue("doMobSpawning", "false");
            Bukkit.getWorlds().get(i).setTime(5000);
        }

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new PluginMessageReceivedListener());

        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "Das LobbySystem wurde erfolgreich gestartet!");
    }


    @Override
    public void onDisable() {
        instance = null;
    }

    private void init() {
        new ConfigManager().load();
        new LocationManager().load();

        Bukkit.getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                // Connect Database
                databaseManager = new DatabaseManager();
                databaseManager.createConnection();

                ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("org.mongodb.driver").setLevel(Level.OFF);
            }
        }, 40);

        // load Inventories
        Data.loadInventories();

        // set friendmanager
        friendManager = new FriendManager();
        friendManager.sendPluginMessage();

        // load PlayerMetaData
        playerMetaData = new PlayerMetaData(this);

        // register commands
        getCommand("setspawn").setExecutor(new SpawnCommand());
        getCommand("setlocation").setExecutor(new SetLocationCommand());
        getCommand("build").setExecutor(new CMD_Build());

        // register Events
        getServer().getPluginManager().registerEvents(new PlayerEventManager(), this);
        getServer().getPluginManager().registerEvents(new WorldEventManager(), this);
        getServer().getPluginManager().registerEvents(new HideEventManager(this), this);
    }

    public static Lobby getInstance() {
        return instance;
    }

    public PlayerMetaData getPlayerMetaData() {
        return playerMetaData;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public FriendManager getFriendManager() {
        return friendManager;
    }
}
