package com.mizzuride.manager;

import com.mizzuride.data.Data;
import com.mizzuride.lobby.Lobby;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigManager {

    private FileConfiguration config = Lobby.getInstance().getConfig();

    public void load() {
        config.options().copyDefaults(true);
        config.addDefault("Prefix", "&c&lMizzuriDE &8| ");
        Lobby.getInstance().saveConfig();
        Data.setPrefix(config.getString("Prefix").replace("&", "§"));
    }

    public FileConfiguration getConfig() {
        return config;
    }

}
