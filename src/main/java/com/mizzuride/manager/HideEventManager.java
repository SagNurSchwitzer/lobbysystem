package com.mizzuride.manager;

import com.mizzuride.commands.CMD_Build;
import com.mizzuride.data.Data;
import com.mizzuride.lobby.Lobby;
import com.mizzuride.utils.InventoryCreator;
import com.mizzuride.utils.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class HideEventManager implements Listener {

    private Lobby instance;
    private List<Player> hideSpamProtection = new ArrayList<>();

    public HideEventManager(Lobby instance) {
        this.instance = instance;
    }

    private void hideAllPlayers(final Player player) {
        for(Player x : Bukkit.getOnlinePlayers()) {
            if(x != player) {
                player.hidePlayer(x);
            }
        }
        player.sendMessage(Data.getPrefix() + "§8Alle Spieler sind nun versteckt!");
    }

    private void showOnlyVips(final Player player) {
        for(Player x : Bukkit.getOnlinePlayers()) {
            if(x != player && !x.hasPermission("mizz.premium")) {
                player.hidePlayer(x);
            } else if(x != player) {
                player.showPlayer(x);
            }
        }
        player.sendMessage(Data.getPrefix() + "§8Du siehst nun nur noch Premium Spieler!");
    }

    private void showAll(final Player player) {
        for(Player x : Bukkit.getOnlinePlayers()) {
            if(x != player) {
                player.showPlayer(x);
            }
        }
        player.sendMessage(Data.getPrefix() + "§8Alle Spieler sind nun wieder sichtbar!");
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event) {
        event.setCancelled(true);

        if(Data.getBuildModeMembers().contains(event.getPlayer())) {
            event.setCancelled(false);
        }

        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if(event.getItem().getItemMeta().getDisplayName() == "§2§lSpieler Status") {
                if(hideSpamProtection.contains(event.getPlayer())) {
                    return;
                }

                hideSpamProtection.add(event.getPlayer());
                Bukkit.getScheduler().scheduleAsyncDelayedTask(instance, new Runnable() {
                    @Override
                    public void run() {
                        hideSpamProtection.remove(event.getPlayer());
                    }
                }, 60);

                Inventory inv = new InventoryCreator("§2§lSpieler Sichtbarkeit", 27).build();
                if(event.getPlayer().hasMetadata("playerVisibility")) {
                    String playerVisibility = instance.getPlayerMetaData().getMetaData(event.getPlayer(), "playerVisibility").get(0).asString();

                    if(playerVisibility.equals("all")) {
                        inv.setItem(11, new ItemCreator(Material.INK_SACK, (byte) 10, 1, "§2§lAlle Spieler anzeigen", Enchantment.DURABILITY).build());
                        inv.setItem(13, new ItemCreator(Material.INK_SACK, (byte) 5, 1, "§6§lNur Premium anzeigen").build());
                        inv.setItem(15, new ItemCreator(Material.INK_SACK, (byte) 1, 1, "§4§lNiemanden anzeigen").build());
                    } else if(playerVisibility.equals("vip")) {
                        inv.setItem(11, new ItemCreator(Material.INK_SACK, (byte) 10, 1, "§2§lAlle Spieler anzeigen").build());
                        inv.setItem(13, new ItemCreator(Material.INK_SACK, (byte) 5, 1, "§6§lNur Premium anzeigen", Enchantment.DURABILITY).build());
                        inv.setItem(15, new ItemCreator(Material.INK_SACK, (byte) 1, 1, "§4§lNiemanden anzeigen").build());
                    } else if(playerVisibility.equals("no")) {
                        inv.setItem(11, new ItemCreator(Material.INK_SACK, (byte) 10, 1, "§2§lAlle Spieler anzeigen").build());
                        inv.setItem(13, new ItemCreator(Material.INK_SACK, (byte) 5, 1, "§6§lNur Premium anzeigen").build());
                        inv.setItem(15, new ItemCreator(Material.INK_SACK, (byte) 1, 1, "§4§lNiemanden anzeigen", Enchantment.DURABILITY).build());
                    }
                } else {
                    instance.getPlayerMetaData().setMetaData(event.getPlayer(), "playerVisibility", "all");
                    inv.setItem(11, new ItemCreator(Material.INK_SACK, (byte) 10, 1, "§2§lAlle Spieler anzeigen", Enchantment.DURABILITY).build());
                    inv.setItem(13, new ItemCreator(Material.INK_SACK, (byte) 5, 1, "§6§lNur Premium anzeigen").build());
                    inv.setItem(15, new ItemCreator(Material.INK_SACK, (byte) 1, 1, "§4§lNiemanden anzeigen").build());
                }
                event.getPlayer().openInventory(inv);
            }
        }
    }

    @EventHandler
    public void onInv(final InventoryClickEvent event) {
        event.setCancelled(true);
        final Player player = (Player) event.getWhoClicked();

        if(event.getInventory().getName().equalsIgnoreCase("§2§lSpieler Sichtbarkeit")) {
            if(event.getCurrentItem() != null) {
                final ItemStack item = event.getCurrentItem();

                if(item.getItemMeta().getDisplayName().equals("§2§lAlle Spieler anzeigen")) {
                    showAll(player);
                    player.closeInventory();
                    instance.getPlayerMetaData().setMetaData(player, "playerVisibility", "all");
                } else if(item.getItemMeta().getDisplayName().equals("§6§lNur Premium anzeigen")) {
                    showOnlyVips(player);
                    player.closeInventory();
                    instance.getPlayerMetaData().setMetaData(player, "playerVisibility", "vip");
                } else if(item.getItemMeta().getDisplayName().equals("§4§lNiemanden anzeigen")) {
                    hideAllPlayers(player);
                    player.closeInventory();
                    instance.getPlayerMetaData().setMetaData(player, "playerVisibility", "no");
                }
            }
        }

        if(Data.getBuildModeMembers().contains(player)) {
            event.setCancelled(false);
        }
    }
}
