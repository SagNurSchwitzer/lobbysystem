package com.mizzuride.manager;

import com.mizzuride.data.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class LocationManager {

    private static final HashMap<String, Location> locations = new HashMap<String, Location>();

    public void load() {
        try {
            locations.put("Spawn", getLocation("SPAWN"));
            locations.put("Minewar", getLocation("MINEWAR"));
            locations.put("Uhcmeetup", getLocation("UHCMEETUP"));
            locations.put("Jumpnrun", getLocation("JUMPNRUN"));

            Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§3Die Spawns wurden erfolgreich geladen!");
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§4Fehler beim laden der Spawn...");
        }
    }

    public void setLocation(String name, Location location) {
        File lobby = new File("plugins//Lobby//lobby.yml");
        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(lobby);

        cfg.set(name.toUpperCase() + ".X", location.getX());
        cfg.set(name.toUpperCase() + ".Y", location.getY());
        cfg.set(name.toUpperCase() + ".Z", location.getZ());
        cfg.set(name.toUpperCase() + ".Yaw", location.getYaw());
        cfg.set(name.toUpperCase() + ".Pitch", location.getPitch());
        cfg.set(name.toUpperCase() + ".World", location.getWorld().getName());
        try {
            cfg.save(lobby);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Location getLocation(String name) {
        try {
            File lobby = new File("plugins//Lobby//lobby.yml");
            YamlConfiguration cfg = YamlConfiguration.loadConfiguration(lobby);
            String mapName = name;

            double x = cfg.getDouble(mapName.toUpperCase() + ".X");
            double y = cfg.getDouble(mapName.toUpperCase() + ".Y");
            double z = cfg.getDouble(mapName.toUpperCase() + ".Z");
            double yaw = cfg.getDouble(mapName.toUpperCase() + ".Yaw");
            double pitch = cfg.getDouble(mapName.toUpperCase() + ".Pitch");
            String world = cfg.getString(mapName.toUpperCase() + ".World");
            return new Location(Bukkit.getWorld(world), x, y, z, (float)yaw, (float)pitch);
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§4Dieser Spawn existiert nicht!");
            return null;
        }
    }

    public static HashMap<String, Location> getLocations() {
        return locations;
    }

}
