package com.mizzuride.manager;

import com.mizzuride.data.Data;
import com.mizzuride.lobby.Lobby;
import com.mizzuride.utils.InventoryCreator;
import com.mizzuride.utils.ItemCreator;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.swing.tree.ExpandVetoException;
import java.util.ArrayList;

public class PlayerEventManager implements Listener {

    private ScoreboardManager scoreboardManager = new ScoreboardManager();

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if(Data.getBuildModeMembers().contains(event.getPlayer())) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if(Data.getBuildModeMembers().contains(event.getPlayer())) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInv(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();

        if(event.getCurrentItem() == null) return;

            final ItemStack clickedItem = event.getCurrentItem();
            if(clickedItem.getItemMeta().getDisplayName().equalsIgnoreCase("§2§lMineWar")) {
                player.teleport(LocationManager.getLocations().get("Minewar"));
            } else if(clickedItem.getItemMeta().getDisplayName().equalsIgnoreCase("§4§lSpawn")) {
                player.teleport(LocationManager.getLocations().get("Spawn"));
            } else if(clickedItem.getItemMeta().getDisplayName().equalsIgnoreCase("§4§lJump'n'run")) {
                player.teleport(LocationManager.getLocations().get("Jumpnrun"));
            } else if(clickedItem.getItemMeta().getDisplayName().equalsIgnoreCase("§5§lUHC-Meetup")) {
                player.teleport(LocationManager.getLocations().get("Uhcmeetup"));
            }

        event.setCancelled(true);

        if (Data.getBuildModeMembers().contains(player)) {
            event.setCancelled(false);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final Location location = LocationManager.getLocations().get("Spawn");
        player.setBedSpawnLocation(location);
        event.setJoinMessage(null);

        Bukkit.getScheduler().runTaskLater(Lobby.getInstance(), new Runnable() {
            public void run() {

                MongoCollection users = Lobby.getInstance().getDatabaseManager().getDatabase().getCollection("users");
                Document document = (Document) users.find(Filters.eq("uuid", player.getUniqueId().toString())).first();

                if(document != null) {
                    Data.getFriendCache().put(player.getName(), (ArrayList<Document>) document.get("friends"));
                    if(document.getInteger("coins") != null) {
                        Data.getCoinsCache().put(player.getUniqueId().toString(), document.getInteger("coins"));
                    }
                } else {
                    Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§8etst");
                }

                player.teleport(location);

                player.getInventory().clear();
                player.setGameMode(GameMode.ADVENTURE);
                player.getInventory().setItem(2, Data.getCompassItem());
                player.getInventory().setItem(6, Data.getHideItem());
                player.getInventory().setItem(4, Lobby.getInstance().getFriendManager().getFriendItem(player));
                player.setHealthScale(6);
                player.setHealth(20);
                player.setLevel(2019);
                player.setFoodLevel(20);

                scoreboardManager.setScoreboard(player);
            }
        }, 0);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if(event.getItem().getItemMeta().getDisplayName() == Data.getCompassItem().getItemMeta().getDisplayName()) {
                event.setCancelled(false);
                event.getPlayer().openInventory(Data.getCompassInventory());
            } else {
                event.setCancelled(true);
            }
        }

        if(Data.getBuildModeMembers().contains(event.getPlayer())) {
            event.setCancelled(false);
        }
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Data.getCoinsCache().remove(event.getPlayer().getUniqueId().toString());
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onAch(PlayerAchievementAwardedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onItemHold(final PlayerItemHeldEvent event) {
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.NOTE_PIANO, 3, 3);
    }
}
