package com.mizzuride.manager;

import com.mizzuride.data.Data;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ScoreboardManager {

    public void setScoreboard(final Player player) {
            Scoreboard scoreboard = new Scoreboard();
            ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
            obj.setDisplayName("§c§lMizzuriDE §8| §9LOBBY");
            PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
            PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);

            ScoreboardScore s1 = new ScoreboardScore(scoreboard, obj, "§7§2              ");
            ScoreboardScore s2 = new ScoreboardScore(scoreboard, obj, "§8Rang:");
            ScoreboardScore s3 = new ScoreboardScore(scoreboard, obj, "§8§l» " + Data.getRankWithColor(player));
            ScoreboardScore s4 = new ScoreboardScore(scoreboard, obj, "§6 ");
            ScoreboardScore s5 = new ScoreboardScore(scoreboard, obj, "§8Coins:");
            ScoreboardScore s6 = new ScoreboardScore(scoreboard, obj, "§8§l» §c§l" + Data.getCoinsCache().get(player.getUniqueId().toString()));
            ScoreboardScore s11 = new ScoreboardScore(scoreboard, obj, "§7§1              ");

            s1.setScore(7);
            s2.setScore(6);
            s3.setScore(5);
            s4.setScore(4);
            s5.setScore(3);
            s6.setScore(2);
            s11.setScore(1);

            PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);
            PacketPlayOutScoreboardScore pa1 = new PacketPlayOutScoreboardScore(s1);
            PacketPlayOutScoreboardScore pa2 = new PacketPlayOutScoreboardScore(s2);
            PacketPlayOutScoreboardScore pa3 = new PacketPlayOutScoreboardScore(s3);
            PacketPlayOutScoreboardScore pa4 = new PacketPlayOutScoreboardScore(s4);
            PacketPlayOutScoreboardScore pa5 = new PacketPlayOutScoreboardScore(s5);
            PacketPlayOutScoreboardScore pa6 = new PacketPlayOutScoreboardScore(s6);
            PacketPlayOutScoreboardScore pa7 = new PacketPlayOutScoreboardScore(s11);



            sendPacket(removePacket, player);
            sendPacket(createPacket, player);
            sendPacket(display, player);

            sendPacket(pa1, player);
            sendPacket(pa2, player);
            sendPacket(pa3, player);
            sendPacket(pa4, player);
            sendPacket(pa5, player);
            sendPacket(pa6, player);
            sendPacket(pa7, player);
    }

    @SuppressWarnings("rawtypes")
    private void sendPacket(Packet packet, Player p) {
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
    }

}
