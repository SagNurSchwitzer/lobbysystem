package com.mizzuride.pluginmessaging;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.mizzuride.data.Data;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class PluginMessageReceivedListener implements PluginMessageListener {



    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if(!channel.equals("BungeeCord")) {
            return;
        }

        try {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subchannel = in.readUTF();
            if(subchannel.equals("PlayerList")) {
                if(in.readUTF().equals("ALL")) {
                    Data.setPlayerList(in.readUTF().split(", "));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
