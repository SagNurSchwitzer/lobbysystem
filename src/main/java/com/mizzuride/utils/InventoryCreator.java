package com.mizzuride.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryCreator {

    private Inventory inventory;
    private String name;

    public InventoryCreator(String name, int size) {
        this.name = name;
        this.inventory = Bukkit.createInventory(null, size, name);
    }

    public InventoryCreator setItem(int index, ItemStack item) {
        this.inventory.setItem(index, item);
        return this;
    }

    public Inventory build() {
        return this.inventory;
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public String getName() {
        return name;
    }
}
