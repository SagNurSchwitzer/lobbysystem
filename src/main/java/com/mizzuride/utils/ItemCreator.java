package com.mizzuride.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemCreator {

    private ItemStack itemStack;
    private Material item;
    private int amount;
    private String displayName;

    public ItemCreator(final Material item, final int amount, final String displayName) {
        this.item = item;
        this.amount = amount;
        this.displayName = displayName;
        ItemStack newStack = new ItemStack(item, amount);
        ItemMeta meta = newStack.getItemMeta();
        meta.setDisplayName(displayName);
        newStack.setItemMeta(meta);
        this.itemStack = newStack;
    }

    public ItemCreator(final Material item, final byte data, final int amount, final String displayName) {
        this.item = item;
        this.amount = amount;
        this.displayName = displayName;
        ItemStack newStack = new ItemStack(item, amount, data);
        ItemMeta meta = newStack.getItemMeta();
        meta.setDisplayName(displayName);
        newStack.setItemMeta(meta);
        this.itemStack = newStack;
    }

    public ItemCreator(final Material item, final byte data, final int amount, final String displayName, Enchantment enchantment) {
        this.item = item;
        this.amount = amount;
        this.displayName = displayName;
        ItemStack newStack = new ItemStack(item, amount, data);
        ItemMeta meta = newStack.getItemMeta();
        meta.setDisplayName(displayName);
        meta.addEnchant(enchantment, 1, true);
        newStack.setItemMeta(meta);
        this.itemStack = newStack;
    }

    public ItemCreator addLore(final List<String> lore) {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        itemMeta.setLore(lore);
        this.itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }

}
