package com.mizzuride.utils;

import com.mizzuride.lobby.Lobby;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class PlayerMetaData {

    private Lobby instance;

    public PlayerMetaData(Lobby instance) {
        this.instance = instance;
    }

    public void setMetaData(final Player player, final String metaDataName, final String metaDataValue) {
        player.setMetadata(metaDataName, new FixedMetadataValue(instance, metaDataValue));
    }

    public void removeMetaData(final Player player, final String metaDataName) {
        if(player.hasMetadata(metaDataName)) {
            player.removeMetadata(metaDataName, instance);
        }
    }

    public List<MetadataValue> getMetaData(final Player player, final String metaDataName) {
        if(player.hasMetadata(metaDataName)) {
            return player.getMetadata(metaDataName);
        }
        return null;
    }

}
